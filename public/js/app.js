const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const messageOne = document.getElementById('msg-1');
const messageTwo = document.getElementById('msg-2');
const date = document.getElementById('date');

const today = new Date();
const d = `${today.getFullYear()}`;
const dateTime = `${d} `;

date.innerHTML = dateTime;

weatherForm.addEventListener('submit', e => {
  e.preventDefault();

  const location = search.value;

  messageOne.textContent = 'Loading...';
  messageTwo.textContent = '';

  fetch(`/weather?address=${location}`).then(response => {
    response.json().then(data => {
      if (data.error) {
        messageOne.textContent = data.error;
      } else {
        messageOne.textContent = data.location;
        messageTwo.textContent = data.forecast;
      }
    });
  });
  search.value = '';
  setInterval(() => {
    messageOne.textContent = '';
    messageTwo.textContent = '';
  }, 5000);
});
