/* eslint-disable camelcase */
const request = require('request');
require('dotenv').config();

const geocode = (address, callback) => {
  const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=${
    process.env.GEOCODE_TOKEN
  }&limit=1`;

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback('Unable to connect to location services!', undefined);
    } else if (body.features.length === 0) {
      callback('Unable to find location. Try another search.', undefined);
    } else {
      const { center, place_name } = body.features[0];

      callback(undefined, {
        latitude: center[0],
        longitude: center[1],
        location: place_name,
      });
    }
  });
};

module.exports = geocode;
